import {Component, ElementRef, ViewChild} from '@angular/core';
import Siema from 'siema';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-scroll-center-example';
  items: any[] = [];
  @ViewChild('containerItems') containerItems: ElementRef;

  ngOnInit() {
	this.items = [1,2,3,4,5,6,7];
  }

  ngAfterViewInit() {
    let nativeElement = this.containerItems.nativeElement;
	nativeElement.scrollLeft = (nativeElement.scrollWidth - nativeElement.clientWidth)/ 2;
  }
}
