import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material';

import { AppComponent } from './app.component';
import { Slider1Component } from './slider1/slider1.component';
import { Slider2Component } from './slider2/slider2.component';

@NgModule({
  declarations: [
    AppComponent,
    Slider1Component,
    Slider2Component
  ],
  imports: [
	BrowserModule,
	MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
