import { Component, OnInit } from '@angular/core';
import Siema from 'siema';

@Component({
  selector: 'app-slider2',
  templateUrl: './slider2.component.html',
  styleUrls: ['./slider2.component.scss']
})
export class Slider2Component implements OnInit {
	siema2: any;
	abc: any;

  constructor() { }

  ngOnInit() {
	this.abc = this.getRandomPics();
  }

  ngAfterViewInit() {
	setTimeout(() => {
		this.siema2 = new Siema({
			selector: '.my-slider2',
			duration: 200,
			easing: 'ease-out',
			perPage: 1,
			startIndex: 0,
			draggable: false,
		  });
	})
  }

  getRandomPics() {
	return ['https://placeimg.com/480/480/any','https://placeimg.com/470/470/any','https://placeimg.com/460/460/any','https://placeimg.com/450/450/any','https://placeimg.com/440/440/any','https://placeimg.com/500/500/any'];
}

  prev() {
	this.siema2.prev();
}

	next() {
	this.siema2.next();
	}

}
