import { Component, OnInit } from '@angular/core';
import Siema from 'siema';

@Component({
  selector: 'app-slider1',
  templateUrl: './slider1.component.html',
  styleUrls: ['./slider1.component.scss']
})
export class Slider1Component implements OnInit {
	abc: any;
	siema: any;

  constructor() { }

  ngOnInit() {
	this.abc = this.getRandomPics();
  }

  ngAfterViewInit() {
	setTimeout(() => {
		this.siema = new Siema({
			selector: '.my-slider',
			duration: 200,
			easing: 'ease-out',
			perPage: 1,
			startIndex: 0,
			draggable: true,
		  });
	})
  }

  getRandomPics() {
	return ['https://placeimg.com/480/480/any','https://placeimg.com/470/470/any','https://placeimg.com/460/460/any','https://placeimg.com/450/450/any','https://placeimg.com/440/440/any','https://placeimg.com/500/500/any'];
}

}
